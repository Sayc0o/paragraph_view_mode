<?php

namespace Drupal\paragraph_view_mode\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldWidget\StringTextfieldWidget;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'paragraph_view_mode' widget.
 *
 * @FieldWidget(
 *   id = "paragraph_view_mode",
 *   label = @Translation("Paragraph view mode"),
 *   field_types = {
 *     "paragraph_view_mode",
 *   }
 * )
 */
class ParagraphViewModeWidget extends StringTextfieldWidget {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return ['view_modes' => self::getAvailableViewModes()];
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $element['view_modes'] = [
      '#type' => 'checkboxes',
      '#title' => t('Available view modes'),
      '#options' => $this->defaultSettings()['view_modes'],
      '#default_value' => $this->getSetting('view_modes'),
      '#required' => FALSE,
      '#min' => 1,
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $settings = $this->getEnabledViewModes();

    if (empty($settings)) {
      $message = $this->t('No view modes enabled, "default" view mode will be used instead.');
    }
    else {
      $message = $this->t('Available view modes: @types', ['@types' => implode(' ', $settings)]);
    }

    $summary[] = $message;

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element = parent::formElement($items, $delta, $element, $form, $form_state);

    $element['value'] = [
      '#title' => $this->t('View mode'),
      '#type' => 'select',
      '#default_value' => isset($items[$delta]->value) ? $items[$delta]->value : 'default',
      '#options' => $this->getEnabledViewModes() ?: ['default' => 'default'],
      '#required' => TRUE,
      '#weight' => 1,
    ];

    return $element;
  }

  /**
   * Getter for available view modes in paragraph entity type.
   *
   * @return array
   *   Associative array of view mode machine names and labels.
   */
  protected static function getAvailableViewModes() {
    return \Drupal::service('entity_display.repository')->getViewModeOptions('paragraph');
  }

  /**
   * Getter for enabled view modes.
   *
   * @return array
   *   Associative array of view mode machine names and labels.
   */
  protected function getEnabledViewModes() {
    $availableViewModes = self::getAvailableViewModes();
    $currentViewModes = array_filter($this->getSetting('view_modes'));

    return array_intersect_key($availableViewModes, $currentViewModes);
  }

}
